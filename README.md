## TEST CINEMA

**Task**  
First part:  
Please use a client/server model (perform the actual calculation on the
server side). Minimum requirements for the calculator:
1) Add/Deduct/Multiply/Divide
2) Square/Root
3) Support for brackets (/[/{
4) Support for display of calculation history to the user (by clicking a Button)

The deliverable should have a decent UI and be as robust as if an
imaginary client orders that calculator. We should not see exception
error pages if we divide by zero etc.. As for technology server side should be based on Java with Spring/Spring boot. Client side should be an HTML with Javascript/JQuery/AngularJS.

Second part:  
As a simple extension to one of the calculator's functions we could
add feature that would allow for calculation of the e^x integral within a given
interval. This shouldn't be however such simple as it sounds. To check
implementation of multi-threaded programming skills of candidates, the calculator should take as input the following values:
1) The interval in which the integral should be calculated
2) Number of threads in which calculations should be performed. Relaying on the assumption that calculating an integral within a given interval is a perfect task for multitasking, each thread calculates individual sub-interval and after that all results are basically merged.
3) Number of sub-intervals into which the interval of integration should be split.

The function should produce either expected results or adequate
warning/error messages when needed.
