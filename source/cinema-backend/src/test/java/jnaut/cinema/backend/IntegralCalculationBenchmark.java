package jnaut.cinema.backend;

import jnaut.cinema.backend.exception.UnknownCalculationException;
import jnaut.cinema.backend.util.IntegralCalculationUtil;
import jnaut.cinema.backend.util.MultiThreadedIntegralUtil;
import jnaut.cinema.backend.util.SingleThreadedIntegralUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author aszydlowski
 */

@Slf4j
class IntegralCalculationBenchmark {

    private IntegralCalculationUtil singleThreadedIntegralUtil;
    private IntegralCalculationUtil multiThreadedIntegralUtil;

    @BeforeEach
    void setUp() {
        singleThreadedIntegralUtil = new SingleThreadedIntegralUtil();
        multiThreadedIntegralUtil = new MultiThreadedIntegralUtil();
    }

    @DisplayName("Single-threaded calculation run")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#integralCalculationTestData")
    void singleThreadedIntegralCalculationTest(double boundA, double boundB, int threads, int intervals) throws UnknownCalculationException {
        double singleThreadedResult = singleThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        LOG.info("Single-threaded calculation result: {}", singleThreadedResult);
    }

    @DisplayName("Multi-threaded calculation run")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#integralCalculationTestData")
    void multiThreadedIntegralCalculationTest(double boundA, double boundB, int threads, int intervals) throws UnknownCalculationException {
        double multiThreadedResult = multiThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        LOG.info("Multi-threaded calculation result: {}", multiThreadedResult);
    }

    @DisplayName("Single/multi-threaded calculations execution times")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#integralCalculationTestData")
    void integralCalculationUtilsPerformanceTest(double boundA, double boundB, int threads, int intervals) throws UnknownCalculationException {
        long start = System.nanoTime();
        singleThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        long stop = System.nanoTime();
        long durationA = stop - start;
        LOG.info("Single-threaded calculation time: {} ns", durationA);
        start = System.nanoTime();
        multiThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        stop = System.nanoTime();
        double durationB = stop - start;
        LOG.info("Multi-threaded calculation time: {} ns", durationB);
    }

}
