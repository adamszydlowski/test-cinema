package jnaut.cinema.backend;

import static jnaut.cinema.backend.util.CalculationPrecisionUtil.assessErrorPercent;
import static jnaut.cinema.backend.util.CalculationPrecisionUtil.formatCalculationError;

import jnaut.cinema.backend.exception.DivisionByZeroException;
import jnaut.cinema.backend.exception.EmptyExpresionException;
import jnaut.cinema.backend.exception.MalformedEquationExpression;
import jnaut.cinema.backend.exception.UnclosedGroupException;
import jnaut.cinema.backend.exception.UnknownCalculationException;
import jnaut.cinema.backend.util.CalculatorUtil;
import jnaut.cinema.backend.util.ShuntingYardUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author aszydlowski
 */
@Slf4j
class BasicCalculatorTest {

    private static final int ERROR_THRESHOLD_PERCENT = 1;
    private CalculatorUtil calculatorUtil;

    @BeforeEach
    void setUp() {
        calculatorUtil = new ShuntingYardUtil();
    }

    @DisplayName("Basic calculator operations test")
    @ParameterizedTest(name = "[{index}] expr={0}, expected result={1}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#basicCalculatorPrecalculatedTestData")
    void basicCalculatorTest(String equation, double expectedResult)
        throws UnclosedGroupException, UnknownCalculationException, DivisionByZeroException, EmptyExpresionException, MalformedEquationExpression {
        LOG.info("Performing calculation: '{}', expected result: {}, error threshold: {}%", equation, expectedResult, ERROR_THRESHOLD_PERCENT);
        double result = calculatorUtil.calculate(equation);
        double error = assessErrorPercent(expectedResult, result);
        LOG.info("Calculation result: {}, error: {}%", result, formatCalculationError(error));
        Assertions.assertTrue(error < ERROR_THRESHOLD_PERCENT);
    }
}
