package jnaut.cinema.backend.util;

import static java.lang.Math.abs;

/**
 * @author aszydlowski
 */
public class CalculationPrecisionUtil {
    public static double assessErrorPercent(double expectedResult, double actualResult) {
        return abs(abs(expectedResult - actualResult) / expectedResult * 100);
    }

    public static String formatCalculationError(double error) {
        return String.format("%.4f", error);
    }
}
