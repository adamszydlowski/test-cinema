package jnaut.cinema.backend;

import static jnaut.cinema.backend.util.CalculationPrecisionUtil.assessErrorPercent;
import static jnaut.cinema.backend.util.CalculationPrecisionUtil.formatCalculationError;

import jnaut.cinema.backend.exception.UnknownCalculationException;
import jnaut.cinema.backend.util.IntegralCalculationUtil;
import jnaut.cinema.backend.util.MultiThreadedIntegralUtil;
import jnaut.cinema.backend.util.SingleThreadedIntegralUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author aszydlowski
 */
@Slf4j
class IntegralCalculationTest {

    private static final int ERROR_THRESHOLD_PERCENT = 1;
    private IntegralCalculationUtil singleThreadedIntegralUtil;
    private IntegralCalculationUtil multiThreadedIntegralUtil;

    @BeforeEach
    void setUp() {
        singleThreadedIntegralUtil = new SingleThreadedIntegralUtil();
        multiThreadedIntegralUtil = new MultiThreadedIntegralUtil();
    }

    @DisplayName("Single/multi-threaded calculations result equality")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @CsvSource({"5, 10, 1, 10000"})
    void integralCalculationUtilsComplianceTest(double boundA, double boundB, int threads, int intervals) throws UnknownCalculationException {
        double singleThreadedResult = singleThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        LOG.info("Single-threaded calculation result: {}", singleThreadedResult);

        double multiThreadedResult = multiThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        LOG.info("Multi-threaded calculation result: {}", multiThreadedResult);

        Assertions.assertEquals(Math.round(singleThreadedResult), Math.round(multiThreadedResult));
    }

    @DisplayName("Single-threaded calculation: result equality to precalculated value")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#integralCalculationPrecalculatedTestData")
    void singleThreadedIntegralCalculationTest(double boundA, double boundB, int threads, int intervals, double expectedResult) throws UnknownCalculationException {
        LOG.info("Expected calculation result: {}", expectedResult);
        LOG.info("Error threshold: {}%", ERROR_THRESHOLD_PERCENT);
        double result = singleThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        LOG.info("Single-threaded calculation result: {}", result);
        double error = assessErrorPercent(expectedResult, result);
        LOG.info("Calculation error: {}%", formatCalculationError(error));
        Assertions.assertTrue(error < ERROR_THRESHOLD_PERCENT);
    }

    @DisplayName("Multi-threaded calculation: result equality to precalculated value")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#integralCalculationPrecalculatedTestData")
    void MultiThreadedIntegralCalculationTest(double boundA, double boundB, int threads, int intervals, double expectedResult) throws UnknownCalculationException {
        LOG.info("Expected calculation result: {}", expectedResult);
        LOG.info("Error threshold: {}%", ERROR_THRESHOLD_PERCENT);
        double result = singleThreadedIntegralUtil.calculateIntegral(boundA, boundB, threads, intervals);
        LOG.info("Multi-threaded calculation result: {}", result);
        double error = assessErrorPercent(expectedResult, result);
        LOG.info("Calculation error: {}%", formatCalculationError(error));
        Assertions.assertTrue(error < ERROR_THRESHOLD_PERCENT);
    }
}
