package jnaut.cinema.backend;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

/**
 * @author aszydlowski
 */
class TestDataProviders {
    static Stream<Arguments> integralCalculationTestData() {
        return Stream.of(
            Arguments.of(5.0, 10.0,  1,   1),
            Arguments.of(5.0, 10.0,  5,   5),
            Arguments.of(5.0, 10.0,  10,  10),
            Arguments.of(5.0, 10.0,  10,  100),
            Arguments.of(5.0, 10.0,  10,  1000),
            Arguments.of(5.0, 10.0,  10,  10000),
            Arguments.of(5.0, 100.0, 100, 100),
            Arguments.of(5.0, 100.0, 100, 1000)
        );
    }

    static Stream<Arguments> integralCalculationPrecalculatedTestData() {
        return Stream.of(
            Arguments.of(5.0,   10.0, 1, 100000, 21878.05263570414),
            Arguments.of(-10.0, 15.0, 1, 100000, 3269017.372426711),
            Arguments.of(-3.0,  7.0,  1, 100000, 1096.583371360091)
        );
    }

    static Stream<Arguments> basicCalculatorPrecalculatedTestData() {
        return Stream.of(
            Arguments.of("1+2", 3),
            Arguments.of("1-2", -1),
            Arguments.of("1+2*6", 13),
            Arguments.of("(1+2)*6", 18),
            Arguments.of("1+2^2", 5),
            Arguments.of("sqrt(16)", 4),
            Arguments.of("5*(16)", 80),
            Arguments.of("5*(10/(3+2))", 10),
            Arguments.of("3+4*2/(1-5)^2^3", 3.001953125),
            Arguments.of("5*[16]", 80),
            Arguments.of("5*{16}", 80),
            Arguments.of("5*([{16}])", 80),
            Arguments.of("(5+6)*(5/[1-3])", -27.5)

        );
    }

}
