package jnaut.cinema.backend.integration;

import static jnaut.cinema.backend.util.CalculationPrecisionUtil.assessErrorPercent;
import static jnaut.cinema.backend.util.CalculationPrecisionUtil.formatCalculationError;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import jnaut.cinema.backend.api.CalculatorController;
import jnaut.cinema.backend.api.dto.CalculatorInputParams;
import jnaut.cinema.backend.api.dto.IntegralInputParams;
import jnaut.cinema.backend.util.CalculatorUtil;
import jnaut.cinema.backend.util.IntegralCalculationUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author aszydlowski
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@DisplayName("API consistency test")
@AutoConfigureMockMvc
@Slf4j
public class ApiConsistencyIT {

    private static final int ERROR_THRESHOLD_PERCENT = 1;
    private final MockMvc mockMvc;
    private final CalculatorUtil calculatorUtil;
    private final IntegralCalculationUtil integralCalculationUtil;
    private final ObjectMapper objectMapper;

    @Autowired
    ApiConsistencyIT(MockMvc mockMvc, CalculatorUtil calculatorUtil, IntegralCalculationUtil integralCalculationUtil, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.calculatorUtil = calculatorUtil;
        this.integralCalculationUtil = integralCalculationUtil;
        this.objectMapper = objectMapper;
    }

    @DisplayName("Basic calculator operations API consistency test")
    @ParameterizedTest(name = "[{index}] expr={0}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#basicCalculatorPrecalculatedTestData")
    void basicCalculatorApiConsistencyTest(String equation) throws Exception {
        LOG.info("Performing calculation: '{}', using service directly", equation);
        Double serviceResult = calculatorUtil.calculate(equation);
        LOG.info("Performing calculation: '{}', using api", equation);
        String apiStringResult = mockMvc.perform(
            post(CalculatorController.URL + "/calculate")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(new CalculatorInputParams(equation)))
        )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        Double apiResult = Double.parseDouble(apiStringResult);
        Assertions.assertEquals(serviceResult, apiResult);
    }

    @DisplayName("Integral calculation API consistency test")
    @ParameterizedTest(name = "[{index}] a={0}, b={1}, threads={2}, intervals={3}")
    @MethodSource("jnaut.cinema.backend.TestDataProviders#integralCalculationPrecalculatedTestData")
    void integralCalculationApiConsistencyTest(double boundA, double boundB, int threads, int intervals) throws Exception {
        Double serviceResult = integralCalculationUtil.calculateIntegral(boundA, boundB, threads, intervals);
        String apiStringResult = mockMvc.perform(
            post(CalculatorController.URL + "/integral")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(new IntegralInputParams(boundA, boundB, threads, intervals)))
        )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        Double apiResult = Double.parseDouble(apiStringResult);
        double error = assessErrorPercent(serviceResult, apiResult);
        LOG.info("Calculation error: {}%", formatCalculationError(error));
        Assertions.assertTrue(error < ERROR_THRESHOLD_PERCENT);
    }
}
