package jnaut.cinema.backend.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author aszydlowski
 */
@Component
@SingleThreaded
@Slf4j
public class SingleThreadedIntegralUtil implements IntegralCalculationUtil {

    @Override
    public double calculateIntegral(double boundA, double boundB, int threads, int subintervals) {
        LOG.debug("Calculating e^x integral in interval ({},{}) with 1 thread and {} subintervals", boundA, boundB, subintervals);
        double dx = (boundB - boundA) / subintervals;
        double integralApprox = 0;
        double current = boundA;
        while (current < boundB) {
            double subBoundB = current + dx;
            integralApprox += calculateSubInterval(current, subBoundB, dx);
            current = subBoundB;
        }
        LOG.debug("{} is the result of calculating of e^x integral in interval ({},{}) with 1 thread and {} subinterals", integralApprox, boundA, boundB, subintervals);
        return integralApprox;
    }

    private double calculateSubInterval(double boundA, double boundB, double dx) {
        return ((Math.exp(boundA) + Math.exp(boundB)) / 2) * dx;
    }
}
