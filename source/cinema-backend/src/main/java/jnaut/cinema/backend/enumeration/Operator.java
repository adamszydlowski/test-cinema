package jnaut.cinema.backend.enumeration;

import lombok.Getter;

/**
 * @author aszydlowski
 */
@Getter
public enum Operator {
    SUBTRACT("-", 0, OperatorAssociativity.LEFT),
    ADD("+", 0, OperatorAssociativity.LEFT),
    DIVIDE("/", 1, OperatorAssociativity.LEFT),
    MULTIPLY("*", 2, OperatorAssociativity.LEFT),
    POWER("^", 3, OperatorAssociativity.RIGHT),
    SQUARE_ROOT("sqrt", 3, OperatorAssociativity.LEFT),
    BRACE_1_START("(", -1, null),
    BRACE_1_END(")", -1, null),
    BRACE_2_START("[", -1, null),
    BRACE_2_END("]", -1, null),
    BRACE_3_START("{", -1, null),
    BRACE_3_END("}", -1, null);

    private final String character;
    private final int precedence;
    private final OperatorAssociativity associativity;

    Operator(String character, Integer precedence, OperatorAssociativity associativity) {
        this.character = character;
        this.precedence = precedence;
        this.associativity = associativity;
    }

    public int comparePrencedenceTo(Operator o) {
        return Integer.compare(this.precedence, o.getPrecedence());
    }


}
