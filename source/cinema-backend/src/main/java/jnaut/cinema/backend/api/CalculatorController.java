package jnaut.cinema.backend.api;

import static jnaut.cinema.backend.api.CalculatorController.URL;

import java.util.List;
import java.util.stream.Collectors;
import jnaut.cinema.backend.api.converter.CalculationHistoryEntryConverter;
import jnaut.cinema.backend.api.dto.CalculationHistoryEntryDto;
import jnaut.cinema.backend.api.dto.CalculatorInputParams;
import jnaut.cinema.backend.api.dto.IntegralInputParams;
import jnaut.cinema.backend.enumeration.CalculationType;
import jnaut.cinema.backend.exception.DivisionByZeroException;
import jnaut.cinema.backend.exception.EmptyExpresionException;
import jnaut.cinema.backend.exception.MalformedEquationExpression;
import jnaut.cinema.backend.exception.UnclosedGroupException;
import jnaut.cinema.backend.exception.UnknownCalculationException;
import jnaut.cinema.backend.service.CalculationHistoryService;
import jnaut.cinema.backend.service.CalculatorService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * Kontroler przyjmujacy zadania obliczenia rownania
 *
 * @author aszydlowski
 */
@RestController
@RequestMapping(URL)
public class CalculatorController {

    private final CalculatorService calculatorService;
    private final CalculationHistoryService calculationHistoryService;
    private final CalculationHistoryEntryConverter calculationHistoryEntryConverter;

    public static final String URL = "/calculator";

    public CalculatorController(CalculatorService calculatorService, CalculationHistoryService calculationHistoryService, CalculationHistoryEntryConverter calculationHistoryEntryConverter) {
        this.calculatorService = calculatorService;
        this.calculationHistoryService = calculationHistoryService;
        this.calculationHistoryEntryConverter = calculationHistoryEntryConverter;
    }

    @PostMapping(path = "/calculate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Double> calculate(@RequestBody CalculatorInputParams in) throws UnclosedGroupException, UnknownCalculationException, DivisionByZeroException, EmptyExpresionException, MalformedEquationExpression {
        Double result = calculatorService.calculate(in.getEquation());
        calculationHistoryService.saveCalculationHistoryEntry(
            RequestContextHolder.currentRequestAttributes().getSessionId(),
            CalculationType.BASIC,
            in.getEquation(),
            result
        );
        return ResponseEntity.ok(result);
    }

    @PostMapping(path = "/integral", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Double> calculateIntegral(@RequestBody IntegralInputParams in) throws UnknownCalculationException {
        Double result = calculatorService.calculateIntegral(in.getLowerBound(), in.getUpperBound(), in.getThreads(), in.getIntervals());
        calculationHistoryService.saveCalculationHistoryEntry(
            RequestContextHolder.currentRequestAttributes().getSessionId(),
            CalculationType.INTEGRAL,
            in.toString(),
            result
        );
        return ResponseEntity.ok(result);
    }

//    @GetMapping(path = "/history/{userId}")
//    public ResponseEntity<List<CalculationHistoryEntryDto>> getCalculationHistory(@PathVariable("userId") String userId) {
//        return ResponseEntity.ok(
//            calculationHistoryService.getCalculationHistory(userId)
//            .map(calculationHistoryEntryConverter::toDto)
//            .collect(Collectors.toList())
//        );
//    }

    @GetMapping(path = "/history")
    public ResponseEntity<List<CalculationHistoryEntryDto>> getCalculationHistory() {
        return ResponseEntity.ok(
            calculationHistoryService.getCalculationHistory(RequestContextHolder.currentRequestAttributes().getSessionId()).stream()
                .map(calculationHistoryEntryConverter::toDto)
                .collect(Collectors.toList())
        );
    }

}
