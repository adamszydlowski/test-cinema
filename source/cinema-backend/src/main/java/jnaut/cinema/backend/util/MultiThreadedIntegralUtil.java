package jnaut.cinema.backend.util;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import jnaut.cinema.backend.exception.UnknownCalculationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @author aszydlowski
 */
@Component
@Slf4j
@Primary
@MultiThreaded
public class MultiThreadedIntegralUtil implements IntegralCalculationUtil {

    private static final ExecutorService executorService = Executors.newFixedThreadPool(8);

    @Override
    public double calculateIntegral(double boundA, double boundB, int threads, int subintervals) throws UnknownCalculationException {
        LOG.debug("Calculating e^x integral in interval ({},{}) with {} threads and {} subintervals", boundA, boundB, threads, subintervals);
        final double dx = (boundB - boundA) / subintervals;
        double current = boundA;

        Set<CompletableFuture<Double>> tasks = new HashSet<>(subintervals);
        for(int i = 0; i < subintervals ; i++) {
            double subBoundB;
            if(i < subintervals - 1) {
                subBoundB = current + dx;
            } else {
                // correction for FP arithmetic precision, last interval must reach the upper bound of the integral
                subBoundB = boundB;
            }
            double finalCurrent = current;
            tasks.add(CompletableFuture.supplyAsync(() -> calculateSubInterval(finalCurrent, subBoundB, dx), executorService));
            current = subBoundB;
        }

        double integralApprox;
        try {
            integralApprox = CompletableFuture.allOf(tasks.toArray(new CompletableFuture[subintervals]))
                .thenApply(aVoid ->
                    tasks.stream()
                        .map(task -> task.getNow(0.0))
                        .reduce(0.0, Double::sum)
                ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new UnknownCalculationException();
        }

        LOG.debug("{} is the result of calculating e^x integral in interval ({},{}) with {} threads and {} subinterals", integralApprox, boundA, boundB, threads, subintervals);
        return integralApprox;
    }

    private double calculateSubInterval(double boundA, double boundB, double dx) {
        return ((Math.exp(boundA) + Math.exp(boundB)) / 2) * dx;
    }
}
