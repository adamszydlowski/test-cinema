package jnaut.cinema.backend.enumeration;

/**
 * @author aszydlowski
 */
public enum CalculationType {
    BASIC,
    INTEGRAL
}
