package jnaut.cinema.backend.api.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import jnaut.cinema.backend.enumeration.CalculationType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author aszydlowski
 */
@Getter
@Setter
@Accessors(chain = true)
public class CalculationHistoryEntryDto implements Serializable {
    private String userId;
    private CalculationType calculationType;
    private String input;
    private Double result;
    private LocalDateTime dateAdded;
}
