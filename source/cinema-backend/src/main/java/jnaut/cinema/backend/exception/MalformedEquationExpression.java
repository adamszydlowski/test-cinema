package jnaut.cinema.backend.exception;

/**
 * @author aszydlowski
 */
public class MalformedEquationExpression extends Exception {

    public MalformedEquationExpression(String message) {
        super(message);
    }
}
