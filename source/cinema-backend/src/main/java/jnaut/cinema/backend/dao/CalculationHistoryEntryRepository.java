package jnaut.cinema.backend.dao;

import java.util.List;
import jnaut.cinema.backend.domain.CalculationHistoryEntryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author aszydlowski
 */
public interface CalculationHistoryEntryRepository extends JpaRepository<CalculationHistoryEntryEntity, Long> {
    List<CalculationHistoryEntryEntity> findByUserId(String userId);
}
