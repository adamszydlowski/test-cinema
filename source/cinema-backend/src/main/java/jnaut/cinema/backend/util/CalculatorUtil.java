package jnaut.cinema.backend.util;

import jnaut.cinema.backend.exception.DivisionByZeroException;
import jnaut.cinema.backend.exception.EmptyExpresionException;
import jnaut.cinema.backend.exception.MalformedEquationExpression;
import jnaut.cinema.backend.exception.UnclosedGroupException;
import jnaut.cinema.backend.exception.UnknownCalculationException;
import org.springframework.stereotype.Component;

/**
 * @author aszydlowski
 */
@Component
public interface CalculatorUtil {
    double calculate(String equation) throws UnclosedGroupException, UnknownCalculationException, MalformedEquationExpression, DivisionByZeroException, EmptyExpresionException;
}
