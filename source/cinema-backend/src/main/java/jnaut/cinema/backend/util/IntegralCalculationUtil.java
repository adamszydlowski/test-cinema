package jnaut.cinema.backend.util;

import jnaut.cinema.backend.exception.UnknownCalculationException;

/**
 * @author aszydlowski
 */
public interface IntegralCalculationUtil {
    double calculateIntegral(double boundA, double boundB, int threads, int intervals) throws UnknownCalculationException;
}
