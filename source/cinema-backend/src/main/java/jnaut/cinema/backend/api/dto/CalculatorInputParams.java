package jnaut.cinema.backend.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author aszydlowski
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CalculatorInputParams {
    private String equation;
}
