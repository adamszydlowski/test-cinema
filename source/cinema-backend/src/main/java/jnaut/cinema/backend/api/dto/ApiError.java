package jnaut.cinema.backend.api.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author aszydlowski
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ApiError implements Serializable {
    private String message;
}
