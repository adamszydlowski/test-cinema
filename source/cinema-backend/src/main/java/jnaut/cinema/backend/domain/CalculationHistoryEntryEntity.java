package jnaut.cinema.backend.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import jnaut.cinema.backend.enumeration.CalculationType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author aszydlowski
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
@ToString
@Table(name = "calculation_history")
public class CalculationHistoryEntryEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    @Setter(AccessLevel.PROTECTED)
    private Long id;

    @Column(name = "user_id")
    private String userId;

    @Enumerated
    @Column(name = "calculation_type")
    private CalculationType calculationType;

    @Column(name = "input")
    private String input;

    @Column(name = "result")
    private Double result;

    @Column(name = "date_added")
    private LocalDateTime dateAdded;
}
