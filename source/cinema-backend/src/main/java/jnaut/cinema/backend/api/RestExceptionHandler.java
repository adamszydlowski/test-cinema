package jnaut.cinema.backend.api;

import jnaut.cinema.backend.api.dto.ApiError;
import jnaut.cinema.backend.exception.DivisionByZeroException;
import jnaut.cinema.backend.exception.EmptyExpresionException;
import jnaut.cinema.backend.exception.UnclosedGroupException;
import jnaut.cinema.backend.exception.UnknownCalculationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Klasa obslugujaca wszystkie przewidziane wyjatki aplikacyjne, ktore moga zostac rzucone przy obsludze wywolan RESTowych
 *
 * @author aszydlowski
 */
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UnclosedGroupException.class)
    public ResponseEntity<Object> handleUnclosedGroupException(UnclosedGroupException ex, WebRequest request) {
        ApiError error = new ApiError("Unclosed group in input");
        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(UnknownCalculationException.class)
    public ResponseEntity<Object> handleUnknownCalculationException(UnknownCalculationException ex, WebRequest request) {
        LOG.error(ex.getMessage(), ex);
        ApiError error = new ApiError("Unknown calculation exception");
        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(DivisionByZeroException.class)
    public ResponseEntity<Object> handleDivisionByZeroException(DivisionByZeroException ex, WebRequest request) {
        LOG.error(ex.getMessage(), ex);
        ApiError error = new ApiError("Division by zero");
        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(EmptyExpresionException.class)
    public ResponseEntity<Object> handleEmptyExpressionException(EmptyExpresionException ex, WebRequest request) {
        LOG.error(ex.getMessage(), ex);
        ApiError error = new ApiError("Empty expression");
        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
