package jnaut.cinema.backend.api.converter;

import jnaut.cinema.backend.api.dto.CalculationHistoryEntryDto;
import jnaut.cinema.backend.domain.CalculationHistoryEntryEntity;
import org.springframework.stereotype.Component;

/**
 * @author aszydlowski
 */
@Component
public class CalculationHistoryEntryConverter {
    public CalculationHistoryEntryDto toDto(CalculationHistoryEntryEntity in) {
        CalculationHistoryEntryDto dto;
        if(in == null) {
            dto = null;
        } else {
            dto = new CalculationHistoryEntryDto()
                .setUserId(in.getUserId())
                .setCalculationType(in.getCalculationType())
                .setDateAdded(in.getDateAdded())
                .setInput(in.getInput())
                .setResult(in.getResult());
        }
        return dto;
    }
}
