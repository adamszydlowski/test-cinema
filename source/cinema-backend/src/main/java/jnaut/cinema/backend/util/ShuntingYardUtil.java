package jnaut.cinema.backend.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import jnaut.cinema.backend.enumeration.Operator;
import jnaut.cinema.backend.enumeration.OperatorAssociativity;
import jnaut.cinema.backend.exception.DivisionByZeroException;
import jnaut.cinema.backend.exception.EmptyExpresionException;
import jnaut.cinema.backend.exception.MalformedEquationExpression;
import jnaut.cinema.backend.exception.UnclosedGroupException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Util which calculates equations written in infix notation by transforming it to postfix Reverse Polish Notation (RPN), which takes into account precedence of operations
 *
 * @author aszydlowski
 */
@Component
@Primary
@Slf4j
public class ShuntingYardUtil implements CalculatorUtil {

    private final Map<String, Operator> operators;

    public ShuntingYardUtil() {
        Operator[] operators = Operator.values();
        this.operators = new HashMap<>(operators.length);
        Arrays.stream(operators).forEach(o -> this.operators.put(o.getCharacter(), o));
    }

    public double calculate(final String equation) throws DivisionByZeroException, UnclosedGroupException, MalformedEquationExpression, EmptyExpresionException {
        if (equation == null || equation.trim().isEmpty()) {
            throw new EmptyExpresionException();
        }

        String normalizedEquation = normalizeEquation(equation);
        List<String> tokens = validateAndTokenizeEquation(normalizedEquation);
        List<Object> outputTokens = toRPN(tokens);

        return calculateRPN(outputTokens);
    }

    /**
     * Calculates the result of input written in Reverse Polish Notation (RPN)
     * @param outputTokens list consisting of input tokens (operators and values)
     * @return the result
     * @throws DivisionByZeroException if division by zero has occurred
     */
    private Double calculateRPN(List<Object> outputTokens) throws DivisionByZeroException {
        Double result;
        Deque<Double> doubles = new LinkedList<>();
        for (Object token : outputTokens) {
            if (token instanceof Double) {
                doubles.push((Double) token);
            } else if (token instanceof Operator) {
                if (!doubles.isEmpty()) {
                    Double val = doubles.pop();
                    Operator op = (Operator) token;
                    switch (op) {
                        case ADD:
                            doubles.push(doubles.pop() + val);
                            break;
                        case SUBTRACT:
                            doubles.push(doubles.pop() - val);
                            break;
                        case MULTIPLY:
                            doubles.push(doubles.pop() * val);
                            break;
                        case DIVIDE:
                            if (val == 0) {
                                throw new DivisionByZeroException();
                            }
                            doubles.push(doubles.pop() / val);
                            break;
                        case POWER:
                            doubles.push(Math.pow(doubles.pop(), val));
                            break;
                        case SQUARE_ROOT:
                            doubles.push(Math.sqrt(val));
                            break;
                        default:
                            throw new RuntimeException(token + " is not an operator or is not supported");
                    }
                }
            } else {
                throw new RuntimeException("Illegal token in the output list: " + token);
            }
        }
        if (doubles.isEmpty() || doubles.size() > 1) {
            throw new IllegalArgumentException("Invalid expression, could not find a result. An operator seems to be absent");
        }
        result = doubles.peek();
        return result;
    }

    private List<Object> toRPN(List<String> tokens) throws UnclosedGroupException, MalformedEquationExpression {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Transforming input '{}' to RPN", String.join(" ", tokens));
        }
        Deque<Operator> stack = new LinkedList<>();
        List<Object> result = new ArrayList<>(tokens.size());
        for (String token : tokens) {
            Double number;
            if ((number = parseDouble(token)) != null) {
                result.add(number);
            } else if (operators.containsKey(token)) {
                Operator operator = operators.get(token);
                Operator topOperator = stack.peek();
                if (EnumSet.of(Operator.BRACE_1_START, Operator.BRACE_2_START, Operator.BRACE_3_START).contains(operator)) {
                    stack.push(operator);
                } else if (EnumSet.of(Operator.BRACE_1_END, Operator.BRACE_2_END, Operator.BRACE_3_END).contains(operator)) {
                    Operator braceStart = findBraceStart(operator);
                    while (topOperator != null && !braceStart.equals(topOperator)) {
                        result.add(topOperator);
                        stack.pop();
                        topOperator = stack.peek();
                    }
                    if (!stack.isEmpty()) {
                        stack.pop();
                    } else {
                        throw new UnclosedGroupException();
                    }
                } else {
                    while (topOperator != null && ((topOperator.comparePrencedenceTo(operator) > 0) || (OperatorAssociativity.LEFT.equals(topOperator.getAssociativity())
                        && topOperator.comparePrencedenceTo(operator) == 0))) {
                        result.add(topOperator);
                        stack.pop();
                        topOperator = stack.peek();
                    }
                    stack.push(operator);
                }
            } else {
                throw new MalformedEquationExpression("Unrecognized token: " + token);
            }
        }

        while (!stack.isEmpty()) {
            Operator op = stack.pop();
            result.add(op);
        }
        if(LOG.isDebugEnabled()) {
            LOG.debug("Infix input: {} transformed to RPN: {}",
                String.join(" ", tokens),
                result.stream()
                .map(o -> {
                    Object out;
                    if(o instanceof Operator) {
                        out = ((Operator) o).getCharacter();
                    } else {
                        out = o;
                    }
                    return out;
                }).map(Object::toString)
                .collect(Collectors.joining(" ")));
        }
        return result;
    }

    private Operator findBraceStart(Operator braceEnd) {
        Operator result;
        switch (braceEnd) {
            case BRACE_1_END:
                result = Operator.BRACE_1_START;
                break;
            case BRACE_2_END:
                result = Operator.BRACE_2_START;
                break;
            case BRACE_3_END:
                result = Operator.BRACE_3_START;
                break;
            default:
                throw new IllegalArgumentException("Invalid brace argument: " + braceEnd);
        }
        return result;
    }

    /**
     * Splits the input into tokens, which can be: - a number (parseable to Double) - an operator (from the list od supported operators, including brackets)
     *
     * @param equation input input
     * @return list of validated tokens
     * @throws MalformedEquationExpression - if any of the characters cannot be transformed into a token
     * @see Operator
     */
    private List<String> validateAndTokenizeEquation(String equation) throws MalformedEquationExpression {
        Pattern pattern = Pattern.compile("((([0-9]*[.])?[0-9]+)|([\\+\\-\\*\\/\\(\\)\\^\\[\\]\\{\\}])|(sqrt))");
        Matcher matcher = pattern.matcher(equation);

        int counter = 0; //must be equal to the index of the end of the last matching group
        List<String> tokens = new ArrayList<>();
        while (matcher.find()) {
            if (matcher.start() != counter) {
                //at this point counter indicates the end of the last matching group. If the next matching group
                //doesn't start at this index, it means that some characters were skipped
                throw new MalformedEquationExpression("Invalid Expression:" + equation + ". Error between " + counter + " end " + matcher.start());
            }
            tokens.add(matcher.group().trim());//add the token if it's okay
            counter += tokens.get(tokens.size() - 1).length();//update the counter
        }
        if (counter != equation.length()) {
            //if the matcher reaches the end of the string, we want to check if the last matching group ends at the end of the expression
            throw new MalformedEquationExpression("Invalid end of expression");
        }
        return tokens;
    }

    /**
     * Parses the String token to
     *
     * @param token input token
     * @return parsed Double value or null if the token is not parseable to Double
     */
    private Double parseDouble(String token) {
        Double result;
        try {
            result = Double.parseDouble(token);
        } catch (NumberFormatException ex) {
            result = null;
        }
        return result;
    }

    /**
     * Normalizes input: - removes whitespace characters - prefixes all negative numbers' sign characters with 0 (inside brackets) - if the input starts with negative number,
     * it's sign character is prefixed with 0 as well
     *
     * @param equation input to normalize
     * @return normalized input
     */
    private String normalizeEquation(String equation) {
        String result = equation.replaceAll("\\s+", "").replace("(-", "(0-");

        if (result.startsWith("-")) {
            result = "0" + equation;
        }

        return result;
    }

}
