package jnaut.cinema.backend.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.transaction.Transactional;
import jnaut.cinema.backend.dao.CalculationHistoryEntryRepository;
import jnaut.cinema.backend.domain.CalculationHistoryEntryEntity;
import jnaut.cinema.backend.enumeration.CalculationType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author aszydlowski
 */
@Service
@Transactional
@Slf4j
public class CalculationHistoryService {

    private final CalculationHistoryEntryRepository calculationHistoryEntryRepository;

    public CalculationHistoryService(CalculationHistoryEntryRepository calculationHistoryEntryRepository) {
        this.calculationHistoryEntryRepository = calculationHistoryEntryRepository;
    }

    @Async
    public CompletableFuture<Void> saveCalculationHistoryEntry(String userId, CalculationType calculationType, String input, Double result) {
        CalculationHistoryEntryEntity historyEntry = new CalculationHistoryEntryEntity()
            .setUserId(userId)
            .setCalculationType(calculationType)
            .setInput(input)
            .setResult(result)
            .setDateAdded(LocalDateTime.now());
        LOG.info("Saving new calculation history entry: {}", historyEntry);
        calculationHistoryEntryRepository.save(historyEntry);
        return CompletableFuture.completedFuture(null);
    }

    public List<CalculationHistoryEntryEntity> getCalculationHistory(String userId) {
        return calculationHistoryEntryRepository.findByUserId(userId);
    }
}
