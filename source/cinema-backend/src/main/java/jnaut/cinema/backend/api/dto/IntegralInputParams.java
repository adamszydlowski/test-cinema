package jnaut.cinema.backend.api.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author aszydlowski
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IntegralInputParams implements Serializable {
    private Double lowerBound;
    private Double upperBound;
    private Integer threads;
    private Integer intervals;
}
