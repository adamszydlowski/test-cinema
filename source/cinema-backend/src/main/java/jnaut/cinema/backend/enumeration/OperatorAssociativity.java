package jnaut.cinema.backend.enumeration;

/**
 * @author aszydlowski
 */
public enum OperatorAssociativity {
    LEFT,
    RIGHT
}
