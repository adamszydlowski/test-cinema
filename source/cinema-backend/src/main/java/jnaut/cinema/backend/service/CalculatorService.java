package jnaut.cinema.backend.service;

import javax.transaction.Transactional;
import jnaut.cinema.backend.exception.DivisionByZeroException;
import jnaut.cinema.backend.exception.EmptyExpresionException;
import jnaut.cinema.backend.exception.MalformedEquationExpression;
import jnaut.cinema.backend.exception.UnclosedGroupException;
import jnaut.cinema.backend.exception.UnknownCalculationException;
import jnaut.cinema.backend.util.CalculatorUtil;
import jnaut.cinema.backend.util.IntegralCalculationUtil;
import jnaut.cinema.backend.util.MultiThreaded;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author aszydlowski
 */
@Service
@Transactional
@Slf4j
public class CalculatorService {

    private final IntegralCalculationUtil integralCalculationUtil;
    private final CalculatorUtil calculatorUtil;

    public CalculatorService(@MultiThreaded IntegralCalculationUtil integralCalculationUtil, CalculatorUtil calculatorUtil) {
        this.integralCalculationUtil = integralCalculationUtil;
        this.calculatorUtil = calculatorUtil;
    }

    public Double calculate(String equation)
        throws UnclosedGroupException, UnknownCalculationException, DivisionByZeroException, EmptyExpresionException, MalformedEquationExpression {
        return calculatorUtil.calculate(equation);
    }

    public Double calculateIntegral(double boundA, double boundB, int threads, int intervals) throws UnknownCalculationException {
        return integralCalculationUtil.calculateIntegral(boundA, boundB, threads, intervals);
    }


}
