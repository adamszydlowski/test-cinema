CREATE TABLE calculation_history (
  id BIGSERIAL PRIMARY KEY,
  user_id VARCHAR(100) NOT NULL,
  calculation_type VARCHAR(10) NOT NULL,
  input VARCHAR(255) NOT NULL,
  date_added TIMESTAMP NOT NULL,
  result DOUBLE PRECISION NOT NULL
);

create index ix_calculation_history_user_id on calculation_history (user_id);